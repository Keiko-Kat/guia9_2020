#include <iostream>
#ifndef NODO_H
#define NODO_H

/* define la estructura del nodo. */
typedef struct _Nodo {
    int numero;
    struct _Nodo *sig = NULL;
} Nodo;

#endif
