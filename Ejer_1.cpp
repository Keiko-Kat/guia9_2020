#include "Ejer_1.h"
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <ctime>
#include <chrono>
#include <math.h>
#include "Nodo.h"
#include "Lista.h"
using namespace std;


int main (int argc, char** argv) {
	//se reciben parametros de terminal al abrir el programa
	//se comprueba que no falten parametros
	if(argc < 2){
		//si los parametros entregados por terminal son menos que los necesarios
	    cout<<"||Faltan parámetros||"<<endl;
	    //se envia un mensaje de error y se cierra el programa
	    return -1;
	}
	//se ingresa el segundo valor dado por terminal a un array de char
	char *opt = argv[1];
    string in;
	
    int opi = static_cast<int>(opt[0]);
    bool is_true, valid_in = false;
    //luego se comprueba la validez del ingreso char del inicio del programa
	while(valid_in != true){
	//opi es igual al valor ASCII de la letra ingresada, y se le compara a los valores de "l" "c" "d" y "e"
		if(opi == 108|| opi == 99 || opi == 100 || opi == 101 ){
			//si el char es valido, el booleano valid_in se vuelve verdadero y cierra el while
			valid_in = true;
		}else{
			//si el char no corresponde, se pide otro ingreso
			cout<<"ingrese nueva opcion valida: (l, c, d, e)"<<endl;
			cout<<"   ~~~> ";
			getline(cin, in);
			//se lee el string y se vuelve un array de char
			opt = const_cast<char*>(in.c_str());
			//se toma el primer char y se guarda su valor ASCII como int 
			opi = static_cast<int>(opt[0]);
			//una vez hecho el while se devuelve a la comprovacion de valores
		}
	}
    
    
    
    Ejercicio e = Ejercicio();
    //se crea un objeto de tipo ejercicio, que contiene las funciones basicas del programa
    
    
    int array[20] = {0} , num, temp = -1, hash, intento, cont = 0;
    Lista *arr[20];
    for(int i = 0; i < 20; i++){
		e.llena(arr[i]);
	}
    
    while(temp != 0){
		//se llama a la funcion menu de "Ejercicio" para generar una opcion valida de funcionamiento
		temp = e.menu();
		
		switch(temp){
			case 1:	cout<<"Ingrese un numero entero positivo:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					//se obtiene un string del teclado y se hace la validacion de int
					is_true = e.validar_int(in);
					//si is_true es verdadero, se transforma el string a int y se ingresa a la funcion insertar perteneciente al arbol
					if(is_true == true){
						num = stoi(in);
						while(num <= 0){
							cout<<"Ingrese un numero entero positivo:"<<endl;
							cout<<"   ~~~> ";
							getline(cin, in);
							//se obtiene un string del teclado y se hace la validacion de int
							is_true = e.validar_int(in);
							//si is_true es verdadero, se transforma el string a int y se ingresa a la funcion insertar perteneciente al arbol
							if(is_true == true){
								num = stoi(in);
							}
						}
						//se calcula el codigo hash del ingreso
						hash = e.Hash(num);
						//y se ve que la posicion no este ocupada
						switch(opi){
							case 108: if(array[hash] == 0){
										array[hash] = num;
									}else{
										if (hash != 19){
											intento = hash +1;
										}else{
											if(hash == 19){
												intento = 0;
											}
										}
										while(intento != hash && cont == 0){
											if (array[intento] == 0){
												array[intento] = num;
												cont = 1;
											}else{
												if(intento == 19){
													intento = 0;
												}else{
													intento = intento +1;
												}
											}
										}
										cont = 0;
									}
													
									break;
								
							case 99: if(array[hash] == 0){
										array[hash] = num;
									}else{
										if (hash != 19){
											intento = hash +1;
										}else{
											if(hash == 19){
												intento = 0;
											}
										}
										cont = 1;
										while(intento != hash && cont != 0){
											if (array[intento] == 0){
												array[intento] = num;
												cont = 0;
											}else{
												if(intento > 19){
													intento = 0;
													cont = 1;
													hash = 0;
												}else{
													cont = cont +1;
													intento = hash + (cont * cont);
												}
											}
										}
									}
									break;
							
							case 100: if(array[hash] == 0){
										array[hash] = num;
									}else{
										while(array[hash] != 0){
											hash = e.Hash_p(hash);
										}
										array[hash] = num;
									}
									break;
								
							case 101: 
									arr[hash]->crear(num);
									break;
										
						}
						if(opi == 101){
							for(int i = 0; i < 20; i++){
								if(arr[i]->raiz == NULL){
									cout<<"-| |-"<<endl;
								}else{
									arr[i]->imprimir();
								}
							}
						}else {
							for(int i = 0; i < 20; i++){
								if(array[i] == 0){
									cout<<"-| |-";
								}else{
									cout<<"-"<<array[i]<<"-";
								}
							}
							cout<<endl;
						}
								
					}else{
						//si is_true es falso se da un mensaje de error y se corta el switch
						cout<<"Ingreso no válido"<<endl;
					}
					break;
			
			case 2:cout<<"Ingrese el numero que busca:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					//se obtiene un string del teclado y se hace la validacion de int
					is_true = e.validar_int(in);
					//si is_true es verdadero, se transforma el string a int y se ingresa a la funcion insertar perteneciente al arbol
					if(is_true == true){
						num = stoi(in);
						switch(opi){
							case 108: e.Prueba_lineal(array, num); 													
									break;
								
							case 99: e.Prueba_cuadratica(array, num);
									break;
							
							case 100: e.Doble_direccion(array, num);
									break;
								
							case 101: e.Encadenamiento(arr, num);
									break;
										
						}
					}break;
					
		}
				
    }
	
   
    return 0;
}
